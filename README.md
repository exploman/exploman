# Exploman


## But du jeu
Vous êtes plusieurs joueurs sur une carte, chaque joueur commence avec la possibilité de poser une bombe par une bombe
Des caisses en bois sont présente sur la map et certaines contiennent des bonus ou des malus. 
Votre objectif sera de réussir à tuer les autres joueurs avant qu'ils ne vous tuent

## Controles
Le joueur 1 est controlé par le clavier et la souris.
 - ```Clavier``` : ```ZQSD``` permet de se déplacer
 - ```Souris``` : Permet de bouger la caméra afin d'avoir une meilleure vision
 - ```Espace``` : Permet de poser la bombe

Les autres joueurs sont controlés par des manettes
 - Le ```joypad de gauche``` permet de se déplacer
 - Le ```joypad de droite``` dirige la caméra
 - Le bouton ```A``` pour switch & ```B``` pour xbox permet de poser la bombe 

