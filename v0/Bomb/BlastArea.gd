extends Area

signal blasted
	
enum {NORTH, SOUTH, WEST, EAST}
var direction = {NORTH: Vector3(0, 0, 5), SOUTH: Vector3(0, 0, -5), WEST: Vector3(5, 0, 0), EAST: Vector3(-5, 0, 0)}
var directionChosen
var blastRange

# dir : Vecteur directionnal de l'explosion
# nb : nombre de case à parcourir
func createBlast(dir, br):
	directionChosen = dir
	blastRange = br

func _physics_process(delta):
	translate((direction[directionChosen])*delta)
	
	if transform.origin.z >= blastRange:
		emit_signal("blasted")
		queue_free()
	if transform.origin.x >= blastRange:
		emit_signal("blasted")
		queue_free()
	if transform.origin.z <= -blastRange:
		emit_signal("blasted")
		queue_free()
	if transform.origin.x <= -blastRange:
		emit_signal("blasted")
		queue_free()

# Gestion des collisions
func _on_BlastArea_body_entered(body):
	if body.is_in_group("BreakableBoxes"):
		# Destruction de la boite
		body.blasted()
		emit_signal("blasted")
		# On retire l'explosion
		queue_free()

	# Gestion collision barrière
	if body.is_in_group("Fences") || body.is_in_group("UnbreakableBoxes"):
		emit_signal("blasted")
		# On retire l'explosion
		queue_free()


