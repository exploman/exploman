extends Spatial

# Declare member variables here. Examples:
var delay = 1
var blasted = 0
# Poseur de la bombe
var player

# Portée de l'explosion de la bombe
var blastRange

var playersStroke = []

# Initilisation des données
# p : Joueur qui pose la bombe
# br : Portée de la bombe du joueur
func init(p, br):
	player = p
	blastRange = br
	
func _physics_process(delta):
	if blasted == 4:
		# All blast have been queue_free
		destroy()

func _on_Bomb_tree_entered():
	var timer = Timer.new()
	timer.set_wait_time(delay)
	timer.one_shot = true
	timer.connect("timeout",self,"_blast") 
	add_child(timer) #to process
	timer.start() #to start

func _blast():
	
	var blastScene = load("res://Bomb/Blast.tscn")
	
	# Play explosion sound
	get_node("AudioStreamExplosion").play()

	
	# Add blasts
	var blastNorth = blastScene.instance()
	add_child(blastNorth)
	blastNorth.connect("blasted", self, "blastExplode")
	blastNorth.connect("body_entered", self, "struckPlayer")
	blastNorth.createBlast(blastNorth.NORTH, blastRange)
	
	var blastSouth = blastScene.instance()
	add_child(blastSouth)
	blastSouth.connect("blasted", self, "blastExplode")
	blastSouth.connect("body_entered", self, "struckPlayer")
	blastSouth.createBlast(blastSouth.SOUTH, blastRange)
	
	var blastWest = blastScene.instance()
	add_child(blastWest)
	blastWest.connect("blasted", self, "blastExplode")
	blastWest.connect("body_entered", self, "struckPlayer")
	blastWest.createBlast(blastWest.WEST, blastRange)
	
	var blastEast = blastScene.instance()
	add_child(blastEast)
	blastEast.connect("blasted", self, "blastExplode")
	blastEast.connect("body_entered", self, "struckPlayer")
	blastEast.createBlast(blastEast.EAST, blastRange)
	
	# Remove the bomb mesh + add exception collision for players
	get_node("Bomb").get_node("BombMesh").visible = false
	for playerInGame in get_tree().get_nodes_in_group("Players"):
		get_node("Bomb").add_collision_exception_with(playerInGame)
	
	player.plantedBombExploded()


func blastExplode():
	blasted += 1

func struckPlayer(body):
	if body.is_in_group("Players"):
		# On diminue la vie du joueur
		if playersStroke.find(body.get_instance_id()) == -1:
			playersStroke.append(body.get_instance_id())
			body.touchedByExplosion()
	
func destroy():
	queue_free()
	
