extends StaticBody

# Quand l'explosion touche la caisse
func blasted():
	var drop = drop()
	if drop:
		drop_item()
	
	queue_free()

func drop():
	var randomizeNumber = RandomNumberGenerator.new()
	randomizeNumber.randomize()
	
	var random = randomizeNumber.randf_range(0.0, 100.0)
	var proba = drop_probability()
	
	if random > proba:
		return true
	else :
		return false

# Calcul la probabilité de drop un objet
func drop_probability():
	var map = get_tree().get_root().get_node("Map")
	var itemsCount = float(map.get_items_count())
	var breakableBoxesCount = float(map.get_breakable_boxes_count())
	var div = float(itemsCount / breakableBoxesCount)
	
	return div * 100.0

# Drop de l'objet
func drop_item():
	var map = get_tree().get_root().get_node("Map")
	var position = transform.origin
	var objectInstance = map.get_random_item()
	
	# Si un objet a été renvoyé
	if objectInstance != null:
		objectInstance.translate(position)
		map.add_child(objectInstance)

