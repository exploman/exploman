extends Spatial

onready var gameOptions = GameOptions.new()
onready var options = gameOptions.load()

var breakableBoxesNumber = 150
var randomizeNumber = RandomNumberGenerator.new()

# Liste de bonus/malus
var items = []

onready var adding_bomb_max = self.options.map.items.adding_bomb.user
onready var removing_bomb_max = self.options.map.items.removing_bomb.user
onready var adding_range_max = self.options.map.items.adding_blast_range.user
onready var removing_range_max = self.options.map.items.removing_blast_range.user
onready var adding_HP_max = self.options.map.items.adding_life.user
onready var adding_shield_max = self.options.map.items.adding_shield.user

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	loadMap()
	prepare_items()

# Permet la generation de la liste d'items de la map
func prepare_items():
	var addingBombScene = load("res://Object/assets/AddingBomb.tscn")
	var removingBombScene = load("res://Object/assets/RemovingBomb.tscn")
	var addingRangeScene = load("res://Object/assets/AddingRange.tscn")
	var removingRangeScene = load("res://Object/assets/RemovingRange.tscn")
	var addingHPScene = load("res://Object/assets/AddingHP.tscn")
	var addingShieldScene = load("res://Object/assets/AddingShield.tscn")
	
	# Ajout du bonus d'ajout de bombe
	for i in range(adding_bomb_max):
		var addingBomb = addingBombScene.instance()
		self.items.push_back(addingBomb)
	
	# Ajout du malus de suppression de bombe
	for i in range(removing_bomb_max):
		var removingBomb = removingBombScene.instance()
		self.items.push_back(removingBomb)
	
	# Ajout du bonus d'augmentation de la portée
	for i in range(adding_range_max):
		var addingRange = addingRangeScene.instance()
		self.items.push_back(addingRange)
	
	# Ajout du malus de la diminution de la portée
	for i in range(removing_range_max):
		var removingRange = removingRangeScene.instance()
		self.items.push_back(removingRange)
	
	for i in range(adding_HP_max):
		var addingHP = addingHPScene.instance()
		self.items.push_back(addingHP)
	
	for i in range(adding_shield_max):
		var addingShield = addingShieldScene.instance()
		self.items.push_back(addingShield)

func loadMap():
	var reservedPositions = []
	# Player1
	reservedPositions.append(Vector3(28, 0, 18))
	reservedPositions.append(Vector3(28, 0, 16))
	reservedPositions.append(Vector3(26, 0, 18))
	#Player2
	reservedPositions.append(Vector3(-28, 0, 18))
	reservedPositions.append(Vector3(-28, 0, 16))
	reservedPositions.append(Vector3(-26, 0, 18))
	# Player3
	reservedPositions.append(Vector3(28, 0, -18))
	reservedPositions.append(Vector3(28, 0, -16))
	reservedPositions.append(Vector3(26, 0, -18))
	#Player4
	reservedPositions.append(Vector3(-28, 0, -18))
	reservedPositions.append(Vector3(-28, 0, -16))
	reservedPositions.append(Vector3(-26, 0, -18))
	
	var breakableBoxScene = load("res://MapElement/BreakableBox.tscn")
	
	# Tableau d'unbreakable boxes
	var unbreakableBoxes = get_tree().get_nodes_in_group("UnbreakableBoxes")
	
	# Tableau de joueurs
	var players = get_tree().get_nodes_in_group("Players")
	
	# Pour chaque breakable box nécessaire
	for i in range(breakableBoxesNumber):
		var breakableBoxes = get_tree().get_nodes_in_group("BreakableBoxes")
		var valid_position = true
		var position = get_random_position()
		
		for j in range(reservedPositions.size()):
			if position == reservedPositions[j]:
				valid_position = false
		
		for j in range(unbreakableBoxes.size()):
			if position == unbreakableBoxes[j].transform.origin:
				valid_position = false
		
		for j in range(breakableBoxes.size()):
			if position == breakableBoxes[j].transform.origin:
				valid_position = false
			
		for j in range(players.size()):
			if position == players[j].transform.origin:
				valid_position = false
		
		if valid_position:
			var breakableBox = breakableBoxScene.instance()
			breakableBox.transform.origin = position
			add_child(breakableBox)

func get_random_position():
	#randomizeNumber.set_seed(OS.get_ticks_msec())
	randomizeNumber.randomize()
	var x = randomizeNumber.randi_range(-14.0, 14.0) * 2
	var z = randomizeNumber.randi_range(-9.0, 9.0) * 2
	
	return Vector3(x, 0, z)

func get_breakable_boxes_count():
	return get_tree().get_nodes_in_group("BreakableBoxes").size()

func get_items_count() :
	return self.items.size()

# Retourne un bonus parmis la liste de bonus restant
func get_random_item():
	
	# S'il reste des objets droppable
	if self.items.size() > 0:
		# On prend un objet de la liste au hasard
		randomizeNumber.randomize()
		var objectIndex = randomizeNumber.randi_range(0, (self.items.size() -1 )/2) *2
		var objectInstance = self.items[objectIndex]
		
		# On retire l'objet choisie
		self.items.remove(objectIndex)
		
		# On retourne l'instance de l'objet choisie
		return objectInstance
	
	# Si tous les objets ont été droppés pendant la partie
	return null
