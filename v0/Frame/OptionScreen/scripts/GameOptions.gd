class_name GameOptions

const FILE_LOCATION = "user://options.sav"

const DEFAULT_OPTIONS_DATA = {
	"blast_range": {
		"default": 2,
		"user": 2
	},
	"bomb": {
		"default": 1,
		"user": 1
	},
	"life": {
		"default": 3,
		"user": 3
	},
	"shield": {
		"default": 0,
		"user": 0
	},
	
	"map": {
		"items": {
			"adding_bomb": {
				"default": 10,
				"user": 10
			},
			"removing_bomb": {
				"default": 5,
				"user": 5
			},
			"adding_life": {
				"default": 5,
				"user": 5
			},
			"adding_shield": {
				"default": 5,
				"user": 5
			},
			"adding_blast_range": {
				"default": 7,
				"user": 7
			},
			"removing_blast_range": {
				"default": 5,
				"user": 5
			}
		}
	}
}

var optionsData

func save(options):
	self.optionsData = options
	
	var saveFile = File.new()
	var err = saveFile.open(FILE_LOCATION, File.WRITE)
	
	print("Saving option to : " + OS.get_user_data_dir())
	
	if err != 0:
		print("Can't save to : " + FILE_LOCATION)
	else:
		saveFile.store_line(to_json(optionsData))
		saveFile.close()

func load():
	var saveFile = File.new()
	
	var err = saveFile.open(FILE_LOCATION, saveFile.READ)
	
	if err == ERR_FILE_NOT_FOUND:
		self.optionsData = self.DEFAULT_OPTIONS_DATA
		save(self.optionsData)
		
	else:
		var text = saveFile.get_as_text()
		self.optionsData = parse_json(text)
		saveFile.close()
	
	return self.optionsData
