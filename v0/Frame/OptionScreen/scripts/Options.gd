extends MarginContainer

onready var gameOptions = GameOptions.new()
onready var options = self.gameOptions.load()

onready var lifeLabel = get_node("MainContainer/OptionsContainer/OptionsChooser/LifeChooser/LifeCount")
onready var bombCountLabel = get_node("MainContainer/OptionsContainer/OptionsChooser/BombChooser/BombCount")
onready var shieldCountLabel = get_node("MainContainer/OptionsContainer/OptionsChooser/ShieldChooser/ShieldCount")
onready var blastRangeCountLabel = get_node("MainContainer/OptionsContainer/OptionsChooser/BlastRangeChooser/BlastRangeCount")

# Called when the node enters the scene tree for the first time.
func _ready():
	lifeLabel.text = String(self.options.life.user)
	bombCountLabel.text = String(self.options.bomb.user)
	shieldCountLabel.text = String(self.options.shield.user)
	blastRangeCountLabel.text = String(self.options.blast_range.user)

func on_option_change():
	lifeLabel.text = String(self.options.life.user)
	bombCountLabel.text = String(self.options.bomb.user)
	shieldCountLabel.text = String(self.options.shield.user)
	blastRangeCountLabel.text = String(self.options.blast_range.user)

func _on_SaveButton_pressed():
	gameOptions.save(self.options)

func _on_ReturnButton_pressed():
	assert(get_tree().change_scene("res://Frame/MainMenu/MainMenu.tscn") == OK)

func _on_DefaultButton_pressed():
	lifeLabel.text = String(self.options.life.default)
	bombCountLabel.text = String(self.options.bomb.default)
	shieldCountLabel.text = String(self.options.shield.default)
	blastRangeCountLabel.text = String(self.options.blast_range.default)
	
	self.options.life.user = self.options.life.default
	self.options.bomb.user = self.options.bomb.default
	self.options.shield.user = self.options.shield.default
	self.options.blast_range.user = self.options.blast_range.default


func _on_LessLife_pressed():
	var life = self.options.life.user
	if life > 0:
		life -= 1
		self.options.life.user = life
		self.on_option_change()

func _on_MoreLife_pressed():
	self.options.life.user += 1
	self.on_option_change()

func _on_LessBombe_pressed():
	var bomb = self.options.bomb.user
	if bomb > 0:
		bomb -= 1
		self.options.bomb.user = bomb
		self.on_option_change()

func _on_MoreBombe_pressed():
	self.options.bomb.user += 1
	self.on_option_change()


func _on_LessShield_pressed():
	var shield = self.options.shield.user
	if shield > 0:
		shield -= 1
		self.options.shield.user = shield
		self.on_option_change()

func _on_MoreShield_pressed():
	self.options.shield.user += 1
	self.on_option_change()


func _on_LessBlastRange_pressed():
	var blastRangeCount = self.options.blast_range.user
	
	if blastRangeCount > 0:
		blastRangeCount -= 1
		self.options.blast_range.user = blastRangeCount
		self.on_option_change()

func _on_MoreBlastRange_pressed():
	self.options.blast_range.user += 1
	self.on_option_change()
