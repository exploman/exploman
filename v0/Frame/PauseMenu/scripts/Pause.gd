extends Popup


func _physics_process(_delta):
	if Input.is_action_just_released("pause"):
		get_tree().paused = true
		show()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_Resume_pressed():
		hide()
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		get_tree().paused = false

func _on_MainMenu_pressed():
	hide()
	get_tree().paused = false
	assert(get_tree().change_scene("res://Frame/MainMenu/MainMenu.tscn") == OK)

