extends MarginContainer

func _on_NewGame_pressed():
	assert(get_tree().change_scene("res://MapElement/Map.tscn") == OK)

func _on_Options_pressed():
	assert(get_tree().change_scene("res://Frame/OptionScreen/Options.tscn") == OK)
