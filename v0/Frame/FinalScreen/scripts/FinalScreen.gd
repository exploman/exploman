extends PopupPanel

# Les joueurs sont append selon leur ordre de mort dans ce tableau
var died = []
var isFinished = false;
onready var ladder = get_node("CenterContainer/Ladder")
onready var nbPlayers = get_tree().get_nodes_in_group("Players").size() 
onready var font = get_node("CenterContainer/Ladder/Title").get_font("font")

func _on_player_died(player):
	if isFinished == false:
		# Append player in the array of died player
		died.append(player)
		addPlayerInLadder(player)
		## If there is only one player alive:
		if died.size() >= get_tree().get_nodes_in_group('Players').size()-1 :
			finished()


func finished():
	isFinished = true
	for player in get_tree().get_nodes_in_group("Players"):
		if player.isDead == false:
			# We get the winner !
			addPlayerInLadder(player, true)
	
	# Game is finished
	get_tree().paused = true
	show()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	
func addPlayerInLadder(player, isWinner = false):
	var position;
	var label = Label.new()
	label.add_font_override("font", font.duplicate())
	
	if isWinner:
		position = 1
		label.get_font("font").size = 30
	else:
		position = nbPlayers+1 - died.size()
		if position < 4 :
			label.get_font("font").size = 16 + (4-position)*3
	
	label.text = "#" + str(position) + " - Player " + str(player.id)
	ladder.add_child(label)
	ladder.move_child(label, 1)


func _on_Menu_Principal_pressed():
	hide()
	get_tree().paused = false
	assert(get_tree().change_scene("res://Frame/MainMenu/MainMenu.tscn") == OK)
