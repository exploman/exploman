extends HBoxContainer

export var id_player = 0
var shield = preload("res://ShieldHolder/assets/shield.png")

func _on_player_shield_collected(shieldValue, id):
	if id == self.id_player:
		update_shield(shieldValue)

func update_shield(shieldValue):
	# Ajout d'un bouclier
	if shieldValue > get_child_count():
		var shieldTexture = TextureRect.new()
		shieldTexture.set_name("shield"+str(shieldValue))
		add_child(shieldTexture)
		shieldTexture.texture = shield
	
	# Suppression d'un bouclier
	else:
		remove_child(get_child(shieldValue))
		pass
	
