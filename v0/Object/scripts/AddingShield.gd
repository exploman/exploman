extends Area

func _on_AddingShieldArea_body_entered(body):
	if body.is_in_group("Players"):
		body.addShield()
		queue_free()
