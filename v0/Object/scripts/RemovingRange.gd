extends Area

func _on_RemovingBombArea_body_entered(body):
	if body.is_in_group("Players"):
		if body.getBlastRange() > 2:
			body.removeBlastRange()
			queue_free()
