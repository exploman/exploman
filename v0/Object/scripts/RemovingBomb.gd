extends Area

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_RemovingBombArea_body_entered(body):
	if body.is_in_group("Players"):
		if body.getNbBombMax() > 1:
			body.removeBombNumber()
			queue_free()
	
	pass
