extends Area

func _on_AddingBombArea_body_entered(body):
	if body.is_in_group("Players"):
		body.addBombNumber()
		queue_free()

