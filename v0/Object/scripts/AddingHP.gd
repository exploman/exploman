extends Area

func _on_AddingHPArea_body_entered(body):
	if body.is_in_group("Players"):
		if body.getLife() < body.startLife:
			body.addHP()
			queue_free()
