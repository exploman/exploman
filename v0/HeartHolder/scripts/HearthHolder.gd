extends HBoxContainer

onready var gameOptions = GameOptions.new()
onready var options = self.gameOptions.load()

export var id_player = 0
var heart_full = preload("res://HeartHolder/assets/hud_heartFull.png")
var heart_empty = preload("res://HeartHolder/assets/hud_heartEmpty.png")

func _ready():
	update_heart(self.options.life.user)
	
func update_heart(value):
	if(value > get_child_count()):
		for i in range(get_child_count(), value):
			var heartFull = TextureRect.new()
			heartFull.set_name("life"+str(i))
			add_child(heartFull)

	for i in get_child_count():
		if value > i:
			get_child(i).texture = heart_full
		else:
			get_child(i).texture = heart_empty

func _on_player_health_changed(life, id):
	if id == self.id_player:
		update_heart(life)
