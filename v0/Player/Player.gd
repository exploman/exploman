extends KinematicBody

# Siagnaux
signal health_changed(life, id)
signal died(player)
signal shield_collected(shield, id)

export var id = 0
var isDead = false

# Options de jeu
onready var gameOptions = GameOptions.new()
onready var options = gameOptions.load()

# Valeurs de départ
export onready var startLife = self.options.life.user
onready var startBomb = self.options.bomb.user
onready var startShield = self.options.shield.user
onready var startBlastRange = self.options.blast_range.user

# Valeurs ingame
onready var lifeCount = self.startLife
onready var bombMaxCount = self.startBomb
onready var bombCount = self.startBomb
onready var shieldCount = self.startShield
onready var blastRangeCount = self.startBlastRange

# Du au fait qu'une case = 2 (Bon c'est mal expliqué ouais...)
var blastRangeMultiplicator = 2

var gravity = Vector3.DOWN * 12  # strength of gravity
var speed = 4  # movement speed
var spin = 0.1  # rotation speed
var velocity = Vector3()
var jump = false
var JOYPAD_SENSITIVITY = 2
const JOYPAD_DEADZONE = 0.15

# Camera
onready var PlayerCamera = get_parent().get_node("CameraSpatial")
onready var PlayerCameraMovable = PlayerCamera.get_node("CameraMovableSpatial")
onready var AudioHitPlayer = get_parent().get_node("AudioHitPlayer")
onready var AudioShieldPlayer = get_parent().get_node("AudioShieldPlayer")

func _physics_process(delta):
	velocity += gravity * delta
	get_input()
	process_rotation_input_joypad()
	velocity = move_and_slide(velocity, Vector3.UP)

func get_input():
	var vy = velocity.y
	velocity = Vector3()
	move()
	if(self.lifeCount >= 0):
		plantBomb()
	velocity.y = vy


func _input(event):
	if id == 1:
		if event is InputEventMouseMotion:
			# Rotate Y mouse
			if event.relative.x > 0:
				rotate_y(-lerp(0, spin, event.relative.x/10))
			elif event.relative.x < 0:
				rotate_y(-lerp(0, spin, event.relative.x/10))

			# Rotate X
			# Limit the camera to go bottom 
			if -event.relative.y >= 0 && PlayerCameraMovable.transform.basis.get_euler().x > -0.3:
				PlayerCameraMovable.rotate_x(-lerp(0, spin, -event.relative.y/100))
			elif -event.relative.y < 0 && PlayerCameraMovable.transform.basis.get_euler().x < 1:
				PlayerCameraMovable.rotate_x(-lerp(0, spin, -event.relative.y/100))


func process_rotation_input_joypad():
	# NOTE: Until some bugs relating to captured mice are fixed, we cannot put the mouse view
	# rotation code here. Once the bug(s) are fixed, code for mouse view rotation code will go here!
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
		
	# ----------------------------------
	# Joypad rotation
	# ----------------------------------
	var joypad_vec = Vector2()
	var camera_vec = Vector2()
	if Input.get_connected_joypads().size() > 0:
		var id_device = id - 2 # Id player start with 1 and first player is controlled by keyboard
		if OS.get_name() == "Windows":
			joypad_vec = Vector2(Input.get_joy_axis(id_device, 2), Input.get_joy_axis(id_device, 3))
			camera_vec = Vector2(Input.get_joy_axis(id_device, 1), Input.get_joy_axis(id_device, 4))
		elif OS.get_name() == "X11":
			joypad_vec = Vector2(Input.get_joy_axis(id_device, 3), Input.get_joy_axis(id_device, 4))
		elif OS.get_name() == "OSX":
			joypad_vec = Vector2(Input.get_joy_axis(id_device, 3), Input.get_joy_axis(id_device, 4))

		if joypad_vec.length() < JOYPAD_DEADZONE:
			joypad_vec = Vector2(0, 0)
		else:
			joypad_vec = joypad_vec.normalized() * ((joypad_vec.length() - JOYPAD_DEADZONE) / (1 - JOYPAD_DEADZONE))
			camera_vec = camera_vec.normalized() * ((camera_vec.length() - JOYPAD_DEADZONE) / (1 - JOYPAD_DEADZONE))

		rotate_y(deg2rad(joypad_vec.x * JOYPAD_SENSITIVITY * -1))
		
		PlayerCameraMovable.rotation.x = clamp(PlayerCameraMovable.rotation.x + deg2rad(joypad_vec.y * JOYPAD_SENSITIVITY/5 * -1), -PI/8, PI/4) 


func move(): 
	if Input.is_action_pressed("move_forward_%s" % id):
		velocity += transform.basis.z * speed
		$AnimationPlayer.play('Run')
	elif Input.is_action_pressed("move_back_%s" % id):
		velocity += -transform.basis.z * speed
		$AnimationPlayer.play('Run')
	elif Input.is_action_pressed("strafe_right_%s" % id):
		velocity += -transform.basis.x * speed
		$AnimationPlayer.play('Run')
	elif Input.is_action_pressed("strafe_left_%s" % id):
		velocity += transform.basis.x * speed
		$AnimationPlayer.play('Run')


##
# Permet le placement de la bombe devant le joueur
##
func calcul_bomb_position(playerPos, bombScene):
	var const_mult = 2 # Multiplicateur car case 2x2
	
	var playerLookX = playerPos.basis.x[0]
	var playerLookZ = playerPos.basis.z[0]
	
	var bombX = int(get_global_transform().origin.x)
	if bombX%2 != 0:
		bombX += 1
	
	var bombZ = int(get_global_transform().origin.z)
	if bombZ%2 != 0:
		bombZ += 1
	
	#On regarde la direction dans laquelle le joueur regarde
	# NORD
	if playerLookX > 0.5 :
		bombZ += 1 * const_mult
	# SUD
	elif playerLookX < -0.5 :
		bombZ -= 1 * const_mult
	
	# OUEST
	elif playerLookZ > 0.5 :
		bombX += 1 * const_mult
	# EAST
	elif playerLookZ < -0.5 :
		bombX -= 1 * const_mult
	
	bombScene.transform.origin.x = bombX
	bombScene.transform.origin.z = bombZ

##
# Posage de bombe
##
func plantBomb():
	var valid_position = true
	
	if Input.is_action_just_released("plant_bomb_%s" % id) && self.bombCount > 0:
		#Plant a bomb
		var bombScene = load("res://Bomb/Bomb.tscn").instance()
		
		calcul_bomb_position(get_global_transform(), bombScene)
		bombScene.rotation = Vector3(0,0,0)
		
		# Récupération d'un vecteur3 contenant la position de la bombe
		var bombPos = Vector3(bombScene.transform.origin.x, 0, bombScene.transform.origin.z)
		
		# On check si on ne place pas la bombe dans une caisse incassable
		var unbreakableBoxes = get_tree().get_nodes_in_group("UnbreakableBoxes")
		for i in range(unbreakableBoxes.size()):
			if bombPos == unbreakableBoxes[i].transform.origin:
				valid_position = false
		
		# On check si on ne place pas la bombe dans une caisse cassable
		var breakableBoxes = get_tree().get_nodes_in_group("BreakableBoxes")
		for i in range(breakableBoxes.size()):
			if bombPos == breakableBoxes[i].transform.origin:
				valid_position = false
		
		# On ne pose la bombe que si la position est libre
		if valid_position:
			get_tree().get_root().get_node("Map").add_child(bombScene)
			
			bombScene.init(self, self.blastRangeCount * blastRangeMultiplicator)
			self.bombCount -= 1
			
			$AnimationPlayer.play('Drop')

##
# Quand une bombe posée a explosée
##
func plantedBombExploded():
	self.bombCount += 1

######
# Effets des objets sur le joueur
######

##
# Bonus d'ajout d'une bombe
##
func addBombNumber():
	self.bombCount += 1
	self.bombMaxCount +=1

##
# Malus de suppression d'une bombe
##
func removeBombNumber():
	self.bombMaxCount -= 1
	if self.bombMaxCount < self.bombCount:
		self.bombCount = self.bombMaxCount

##
# Bonus d'augmentation de la portée de la bombe
##
func addBlastRange():
	self.blastRangeCount += 1 * blastRangeMultiplicator

##
# Bonus de diminution de la portée de la bombe
##
func removeBlastRange():
	self.blastRangeCount -= 1 * blastRangeMultiplicator

##
# Bonus de récupération d'HP
##
func addHP():
	self.lifeCount += 1
	emit_signal("health_changed", self.lifeCount, id)

##
# Bonus de bouclier
##
func addShield():
	self.shieldCount += 1
	emit_signal("shield_collected", self.shieldCount, id)

######
# Getters
######

##
# Récupération du nombre de bombe maximum du joueur
##
func getNbBombMax():
	return self.bombMaxCount

##
# Récupération de la portée des bombes du joueur
##
func getBlastRange():
	return self.blastRangeCount

##
# Récupération de la vie du joueur
##
# USELESS ?
func getLife():
	return self.lifeCount


func touchedByExplosion():
	if self.shieldCount > 0:
		self.shieldCount -= 1
		emit_signal("shield_collected", self.shieldCount, id)
		AudioShieldPlayer.play()
	else:
		setLifeRemaining(self.lifeCount - 1)
		AudioHitPlayer.play()

######
# Life control
######
func setLifeRemaining(newLife):
	self.lifeCount = newLife
	emit_signal("health_changed", self.lifeCount, id)
	if self.lifeCount == 0:
		died()

func died():
	if isDead == false:
		self.isDead = true
		emit_signal("died", self)
	
	var parent = get_parent()
	var defeatScene = load("res://Frame/DefeatPopUp/DefeatPopUp.tscn")
	parent.add_child(defeatScene.instance())
	
	# Make it transparent
	var mat = SpatialMaterial.new()
	mat.albedo_color = Color(1,1,1,0.25)
	mat.flags_transparent = true
	get_node("Skeleton/Mesh").set_surface_material(0, mat)
	
	# Remove its collisionshape with other player
	for player in get_tree().get_nodes_in_group("Players"):
		add_collision_exception_with(player)
